/**
 * Fetch specific google sheet tsv file
 * @returns {String} 
 */
const fetchTSV = async () => {
  stream = await fetch('https://docs.google.com/spreadsheets/d/e/2PACX-1vTOux4HOOluXsOqAEifDSaaUtRQI8Q8jMqg_j-IC4iaCkv1z4PJo7Vjesmy31wO6v7-OQYSVwnUb5qE/pub?output=tsv');
  return await stream.text();
}

/**
 * Create a 2 dimensional array given a TSV string 
 * @param {String} text A string representation of a TSV document 
 * @return {Array}
 */
const tsvToArray = text => text.split('\n').map(row => row.split('\t'));

/**
 * Return an object representation of the expense data 
 * @param {Array} data output of tsvToArray()
 * @returns {Object}
 */
const parseExpenseData = data => {
  return data.map((row) => {
    return { 
      date: formatDate(row[0]), 
      value: parseFloat(row[1].split('@')[0]),
      category: row[1].split('@')[1],
      description: row[1].split('@')[2],
    };
  });
}

/**
 * Create a javascript timestamp given timestamps that are arbitrarily (but uniformly) formated
 * @param {String} str A representation of time in the following format: `January 03, 2019 at 09:11PM`  
 * @return {Date}
 */
const formatDate = (str) => {
  const [ month, day, year, at, time ] = str.split(' ');
  return new Date(`${month} ${day} ${year} ${formatTime(time)}`);
}

/**
 * Create the hours/min/sec segment of a javascript timestamp formatted by an IFTTT applet
 * @param {String} str A representation of time in the following format: `09:11PM` 
 * @return {String}
 */
const formatTime = str => {
  // Hour
  let hour = parseInt(str.slice(0, -3).split(':')[0]);
  if (str.slice(-3) === 'PM')
    hour += 12;
  hour = (hour < 10) ? `0${hour}` : `${hour}`

  // Minutes
  let min = str.slice(0, -3).split(':')[1];
  return `${hour}:${min}:00`;
}

/**
 * Visualize expense data
 * @param {Object} data 
 */
const graphExpenses = (data) => {
  d3.select('.chart')
    .selectAll('div')
    .data(data)
      .enter()
      .append('div')
      .style('width', d => `${d.value * 25}px`)
      .style('background-color', '#5599DD')
      .style('margin', '5px')
      .style('padding', '5px')
      .style('color', 'white')
      .style('border-radius', '5px')
      .text(d => `${d.date.getMonth()+1}/${d.date.getDate()}`)
}

/**
 * Fetch a google sheet containing expense data, and parse it into a JS object
 * @todo handle malformed data
 */
const main = async () => {
  const tsv = await fetchTSV();
  const data = tsvToArray(tsv);
  const parsed = parseExpenseData(data);
  graphExpenses(parsed);
}

main();
